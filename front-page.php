<?php
// Template Name: Página inicial
?>

<?php get_header(); ?>


    <main>
        <section class="adote">
            <figure>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/adote4.jpg" alt="Imagem Lobo" class="imgAdote">
            </figure>
            <div>
                <h1><?php the_field('titulo');?></h1>
                <hr>
                <p><?php the_field('descricao_titulo');?></p>
            </div>
        </section>
        <section class="sobre">
            <h2><?php the_field('sobre');?></h2>
            <p><?php the_field('descricao_sobre');?></p>
        </section>
        <section class="valores">
            <h2><?php the_field('valores_valores');?></h2>
            <div class="cardsDispFlex"> 
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-protecao.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_1');?></h4>
                    <p><?php the_field('valores_descricao_valor_1');?></p> 
               </div>
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-carinho.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_2');?></h4>
                    <p><?php the_field('valores_descricao_valor_2');?></p>    
                </div>
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-companheirismo.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_3');?></h4>
                    <p><?php the_field('valores_descricao_valor_3');?></p>    
                </div>
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-resgate.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_4');?></h4>
                    <p><?php the_field('valores_descricao_valor_4');?></p> 
                </div>
            </div>
        </section>


        <section class="exemplo">
            <h2>Lobos Exemplo</h2>

            <?php $the_query = new WP_Query ('posts_per_page=2'); ?>

            <?php $numero = 0 ?>

                <?php while($the_query -> have_posts()) : $the_query -> the_post(); ?>

                <?php if($numero % 2 == 0){ ?>

                <div class="dispExImpar">
                    <div class="quadroAzulImpar">
                        <?php if(get_field('lobo_foto')): ?>
                            <img src="<?php the_field('lobo_foto');?>" alt="" class="imgExemploImpar">
                        <?php endif; ?>
                    </div>
                    <div class="txtExemplo">
                        <h3><?php the_field('lobo_nome');?></h3>
                        <h5>Idade: <?php the_field('lobo_idade');?> anos</h5>
                        <p><?php the_field('lobo_descricao');?></p>
                    </div>
                </div>
                <?php } else { ?>


                <div class="dispExPar">
                    <div class="txtExemplo">
                        <h3><?php the_field('lobo_nome');?></h3>
                        <h5>Idade: <?php the_field('lobo_idade');?> anos</h5>
                        <p><?php the_field('lobo_descricao');?></p>
                    </div>
                    <div class="quadroAzulPar">
                        <?php if(get_field('lobo_foto')): ?>
                            <img src="<?php the_field('lobo_foto');?>" alt="" class="imgExemploPar">
                        <?php endif; ?>
                    </div>
                </div>

                <?php }
                    $numero++; 
                ?>

                <?php 
                    endwhile;
                    wp_reset_postdata();
                ?>

        </section>

    </main>

<?php get_footer(); ?>