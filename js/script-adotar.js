const urlParams = new URLSearchParams(window.location.search);
const idLobo = urlParams.get('lobo');
const url = `https://lobinhos.herokuapp.com/wolves/${idLobo}`
const containerMensage = document.querySelector(".containerLobo")


function createInfos(name, image_url){
    let nameLobo = document.createElement("span")
    nameLobo.classList.add("nameLobo");
    nameLobo.innerHTML = `${name} <br> <p style="font-size:rem;"> ID: ${idLobo} </h1> `

    let imgLobo = document.createElement("img")
    imgLobo.classList.add("img");
    imgLobo.setAttribute('src', image_url);

    containerMensage.appendChild(imgLobo);
    containerMensage.appendChild(nameLobo);
}

function getApi(url){
    let fetchConfig = {
        method: "GET"
    }

    fetch(url, fetchConfig)
        .then(response => response.json()
        .then(lobo => {
            createInfos(lobo.name, lobo.image_url)
            })
            .catch(error => {console.log(error)})
        )
        .catch(error => {console.log(error)})
}

function putApi(adopter_name, adopter_age, adopter_email) {
    let fetchBody = {
        adopter_name: adopter_name,
        adopter_age: adopter_age,
        adopter_email: adopter_email,
        adopted: true,
    }

    let fetchConfig = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(fetchBody)
    }

    fetch(url, fetchConfig)
        .then(resposta => resposta.json()
            .then(resp => { console.log(resp) })
            .catch(error => { console.log(error) })
        )
        .catch(error => { console.log(error) })

    alert("Adotado com sucesso!")
}

const nome = document.querySelector('#nome')
const idade = document.querySelector('#idade')
const email = document.querySelector('#email')

function adotar() {
    putApi(nome.value, idade.value, email.value);
    window.location.href = 'index.html'
    }

getApi(url)