const url = `https://lobinhos.herokuapp.com/wolves/`

const idade = document.querySelector('#idade');
const nome = document.querySelector('#nome');
const descricao = document.querySelector('#descricao');
const url_imagem = document.querySelector('#link');



function postLobo(age, description, image_url, name){
    let fetchBody = {
        age: age,
        description: description,
        image_url: image_url,
        name: name,
    }

    let fetchConfig = {
        method : "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(fetchBody)
    }

    fetch(url, fetchConfig)
    .then(resposta => resposta.json()
        .then(resp => {console.log(resp)})   
        .catch(error => {console.log(error) })
        )
    .catch(error => {console.log(error)})

    alert("Adicionado com sucesso!")
}

function salvar() {
    postLobo(idade.value, descricao.value, url_imagem.value, nome.value);
    window.location.href = 'index.html'
}