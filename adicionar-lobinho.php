<?php
// Template Name: Adicionar lobinho
?>
<?php get_header(); ?>

<main>
<div class="container">
        <div class="boxMaior">
        <div class="estiloTituloPrincipal"> Coloque um Lobinho para adoção</div>
        <div class="box">


            <div class="divNome">
                <div class="estiloTituloNome"> Nome do Lobinho:</div>
                <input type="text" id="nome" class="estiloNome">
            </div>

            <div class="divAnos">
                <div class="estiloTituloAnos">Anos:</div>
                <input type="number" id="idade" class="estiloAnos">
            </div>

            <div class="divLink">
                <div class="estiloTituloLinkF"> Link da foto:</div>
                <input type="text" id="link" class="estiloLinkF">
            </div>

            <div class="divDescr">
                <div class="estiloTituloDescrição">Descrição:</div>
                <textarea rows="7"></textarea>
            </div>

        </div>
        <button onclick="salvar()">Salvar</button>

        </div>
  </main>
<?php get_footer(); ?>