<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div>
            <a href="/lista-dos-lobinhos" class="linkHeader">Nossos Lobinhos</a>
        </div>
        <figure>
            <img src="<?php echo get_stylesheet_directory_uri() ?>/logos/logo-lobo.png" alt="Logo" class="imgLogo">
        </figure>
        <div class="cxQSMaior">
            <div class="cxQSMenor">
                <a href="/quem-somos" class="linkHeader">Quem Somos</a>
            </div>
        </div>
    </header>