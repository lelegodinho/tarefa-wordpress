<?php
// Template Name: Quem Somos
?>
<?php get_header(); ?>

    <main>
            <div class="container">
            <div class="Titulo"> <?php the_title(); ?> </div>
            <div class="boxMaior">   
         <div class="texto"> <?php the_content(); ?> </div>
            </div>  
    
          </div>
         
         </main>

<?php get_footer(); ?>