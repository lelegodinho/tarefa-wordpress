<hr class="hrFooter">
<footer>
        <div class="footerDispFlex">
            <div class="map">
                <iframe src="https://maps.google.com/maps?q=av.%20milton%20tavares%20souza%20s/n,%20Boa%20Viagem&t=&z=13&ie=UTF8&iwloc=&output=embed" width="300" height="200" class="mapCanvas"></iframe>
            </div>
            <div class="contato">
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/logos/logo-localiz.png" alt="Local">
                    <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210315</p>    
                </div>
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/logos/logo-tel.png" alt="Tel">
                    <p>(99) 99999-9999</p>
                </div>
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/logos/logo-msg.png" alt="E-mail">
                    <p>salve-lobos@lobINhos.com</p>
                </div>
                <nav class="displayQuemSomos">
                    <a class="quemSomos" href="http://adote-um-lobinho.local/quem-somos/">
                        <p>Quem Somos</p>
                    </a>
                </nav>
            </div>
            <div class="pegada">
                <p>Desenvolvido com</p>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/logos/pegada-coracao.png" alt="">
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
    </html>
</body>