<?php
// Template Name: Página inicial
?>

<?php get_header(); ?>


    <main>
        <section class="adote">
            <figure>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/adote4.jpg" alt="Imagem Lobo" class="imgAdote">
            </figure>
            <div>
                <h1><?php the_field('titulo');?></h1>
                <hr>
                <p><?php the_field('descricao_titulo');?></p>
            </div>
        </section>
        <section class="sobre">
            <h2><?php the_field('sobre');?></h2>
            <p><?php the_field('descricao_sobre');?></p>
        </section>
        <section class="valores">
            <h2><?php the_field('valores_valores');?></h2>
            <div class="cardsDispFlex"> 
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-protecao.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_1');?></h4>
                    <p><?php the_field('valores_descricao_valor_1');?></p> 
               </div>
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-carinho.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_2');?></h4>
                    <p><?php the_field('valores_descricao_valor_2');?></p>    
                </div>
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-companheirismo.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_3');?></h4>
                    <p><?php the_field('valores_descricao_valor_3');?></p>    
                </div>
                <div class="cardValores">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/home-page/logo-resgate.png" alt="">
                    <h4><?php the_field('valores_titulo_valor_4');?></h4>
                    <p><?php the_field('valores_descricao_valor_4');?></p> 
                </div>
            </div>
        </section>


        <section class="exemplo">
            <h2>Lobos Exemplo</h2>

            <section class="containerLobosEx">
                <div>
                    <p>

                    </p>
                </div>
            </section>



            <div class="dispExImpar">
                <div class="quadroAzulImpar">
                    <?php if( get_field('lobo_foto') ): ?>
                        <img src="<?php the_field('lobo_foto');?>">
                    <?php endif; ?>
                </div>
                <div class="txtExemplo">
                    <h3 class="nomeLoboImpar"><?php the_field('lobo_nome');?></h3>
                    <h5 class="idadeLoboImpar"><?php the_field('lobo_idade');?> anos</h5>
                    <p class="descrLoboImpar"><?php the_field('lobo_descricao');?></p>
                </div>
            </div>



            <div class="dispExPar">
                <div class="txtExemplo">
                    <h3 class="nomeLoboPar">Nome do Lobo</h3>
                    <h5 class="idadeLoboPar">Idade: XX anos</h5>
                    <p class="descrLoboPar">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laboriosam debitis a saepe inventore animi blanditiis quaerat aspernatur placeat porro velit. Odio eum ea quia. Nam consequatur nemo vel nostrum repellat?</p>
                </div>
                <div class="quadroAzulPar">
                </div>

            </div>
        </section>
    </main>

<?php get_footer(); ?>