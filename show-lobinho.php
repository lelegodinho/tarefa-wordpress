<?php
// Template Name: Show lobinho
?>
<?php get_header(); ?>
    <main >

    <h1><?php the_field('lobo_nome');?></h1>
        <div class="dispEx">   
            <div>
                <div class="quadroAzul">
                    <div class="imgExemplo">
                        <?php if(get_field('lobo_foto')): ?>
                            <img src="<?php the_field('lobo_foto');?>">
                        <?php endif; ?>
                        <div class="botoes">
                            <input type="button" class="adotar" value="ADOTAR" onclick="adotar(this)">
                            <input type="button" class="excluir" vaLUe="EXCLUIR" onclick="excluir(this)">
                        </div>
                    </div>
                </div>  
                <div class="margemPBtns">
                </div> 
            </div>             
            <div class="txtExemplo">
                <p><?php the_field('lobo_descricao');?></p>
            </div>
        </div>
    </main>
<?php get_footer(); ?>



