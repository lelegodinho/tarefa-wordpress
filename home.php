<?php
// Template Name: Lista dos lobinhos
?>

<?php get_header(); ?>
 
    <main class="buscaLobinhos">
        <section class="areaBusca">
            <div class="busca">
                <button class="btnBusca" onclick="buscar()"><img src="<?php echo get_stylesheet_directory_uri() ?>/imgs/search.png"></button>
                <input type="text" id="barraBusca">
                <a href="adicionar-lobinho"><input type="button" class="addLobo" value="+ Lobo"></a>
            </div>
            <label class="adotados">
                <input type="checkbox" id="checkbox"> Ver lobinhos adotados
            </label>
        </section>

        <section class="exemplo">


            <?php $numero = 0 ?>

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <?php if($numero % 2 == 0){ ?>

                <div class="dispExImpar">
                    <div class="quadroAzulImpar">
                        <?php if(get_field('lobo_foto')): ?>
                            <img src="<?php the_field('lobo_foto');?>" alt="" class="imgExemploImpar">
                        <?php endif; ?>
                    </div>
                    <div class="txtExemplo">
                        <h3><?php the_field('lobo_nome');?></h3>
                        <h5>Idade: <?php the_field('lobo_idade');?> anos</h5>
                        <p><?php the_field('lobo_descricao');?></p>
                        <input type="button" class="adotar" value="Adotar" onclick="adotar(this)">
                    </div>
                </div>
                <?php } else { ?>


                <div class="dispExPar">
                    <div class="txtExemplo">
                        <h3><?php the_field('lobo_nome');?></h3>
                        <h5>Idade: <?php the_field('lobo_idade');?> anos</h5>
                        <p><?php the_field('lobo_descricao');?></p>
                        <input type="button" class="adotar" value="Adotar" onclick="adotar(this)">
                    </div>
                    <div class="quadroAzulPar">
                        <?php if(get_field('lobo_foto')): ?>
                            <img src="<?php the_field('lobo_foto');?>" alt="" class="imgExemploPar">
                        <?php endif; ?>
                    </div>
                </div>

                <?php }
                    $numero++; 
                ?>

                <?php endwhile; else:?>
                    <p> Desculpe, o post não segue os critérios escolhidos </p>
                <?php endif; ?>

            <?php my_pagination(); ?>
        </section>
        
    </main>

<?php get_footer(); ?>