<?php
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
?>

<?php
function my_pagination() {
global $wp_query;

echo paginate_links( array(
    'base' => str_replace( 9999999999999, '%#%', esc_url( get_pagenum_link( 9999999999999 ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var( 'paged' ) ),
    'total' => $wp_query->max_num_pages,
    'type' => 'list',
    'prev_next' => true,
    'prev_text' => 'Página Anterior',
    'next_text' => 'Próxima Página',
    'before_page_number' => '-',
    'after_page_number' => '>',
    'show_all' => false,
    'mid_size' => 3,
    'end_size' => 1,
) );
}
?>


<?php
    function css() {
        if(is_page('adicionar-lobinho')) {
            wp_enqueue_style('adicionar-lobinho', get_template_directory_uri().'/css/style-adicionar.css');
        }
        else if(is_page('show-lobinho')) {
            wp_enqueue_style('show-lobinho', get_template_directory_uri().'/css/style-show.css');
        }
        else if(is_page('adotar-lobinho')) {
            wp_enqueue_style('adotar-lobinho', get_template_directory_uri().'/css/style-adotar.css');
        }
        else if(is_page('quem-somos')) {
            wp_enqueue_style('quem-somos', get_template_directory_uri().'/css/style-quem-somos.css');
        }
        else{
            wp_enqueue_style('front-page', get_template_directory_uri().'/css/style-front-home.css');
        }
    }


    add_action('wp_enqueue_scripts', 'css');
?>

<?php
    function js() {
        if(is_page('lista-dos-lobinhos')) {
            wp_enqueue_script('lista-lobinho', get_template_directory_uri().'/js/script-lista.js');
        }
        else if(is_page('adicionar-lobinho')) {
            wp_enqueue_script('adicionar-lobinho', get_template_directory_uri().'/js/script-adicionar.js');
        }
        else if(is_page('show-lobinho')) {
            wp_enqueue_script('show-lobinho', get_template_directory_uri().'/js/script-show.js');
        }
        else if(is_page('adotar-lobinho')) {
            wp_enqueue_script('adotar-lobinho', get_template_directory_uri().'/js/script-adotar.js');
        }
        else if(is_page('quem-somos')) {
            wp_enqueue_script('quem-somos', get_template_directory_uri().'/js/script-quem-somos.js');
        }
        else{
            wp_enqueue_script('home-page', get_template_directory_uri().'/js/script-home.js');
        }
    }

    add_action('wp_enqueue_scripts', 'js');
?>



